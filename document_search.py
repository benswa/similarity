# Benson Liu
# bliu13@ucsc.edu
#
# CMPS 5P, Spring 2014
# Assignment 7
#
# This program searches through documents to find key words that
# specified by the user. Once the words are found, its importance
# is calculated.

__author__ = 'bliu13'

from string import punctuation
from math import log, sqrt
from sys import argv


def get_config_name():
    """
    This function is called in the beginning to get the parameters inputted
    by the user when running this program from the command line.
    """
    argument = argv
    length_of_arguments = len(argument)
    if length_of_arguments > 2:
        print('Error, too many arguments. Usage: document_search.py <config file name>.')
        exit(1)
    if length_of_arguments < 2:
        print('Error, no argument for the config file. Usage: document_search.py <config file name>.')
        exit(1)
    return argument[1]


def process_user_input():
    """
    This function takes will prompt the user for a string of words and will
    return a dictionary full of those words with their punctuation stripped
    and lower cased. This function will proceed to act like the function
    process_book() where the word are counted and are stored in a sub-dictionary
    within a dictionary is returned. The reason we need to do something so insane
    is because we need to create the same data structure as what is outputted by
    process_book() so that we have access to all the functions that were created
    to work with that type of data structure.

    Data Structure: outer_dictionary{inner_dictionary: {'user_inputted_word': word_count}}

    The inner dictionary is formed when it takes in the user's input then at the
    very end, the outer dictionary is created to contain the inner dictionary.

    This function does not guard against situations where numbers are embedded
    in words. The numbers are simply omitted.
    """

    word_count = {}
    while True:
        try:
            user_input = str(input('What is your search text? '))
        except ValueError:
            print('Error: Something strange with your input, try again or try something different.\n')
            continue
        if user_input is '':
            continue

        # Formats the line by removing the punctuation and lower casing everything
        formatted_input = delete_punctuation(user_input.lower())
        word = ''

        # Loops through the line over each character at a time
        input_length = len(formatted_input)
        for letter_index in range(input_length):
            if 'a' <= formatted_input[letter_index] <= 'z':
                # If current character the index is on in the line is a
                # letter, then add the letter to the word
                word += formatted_input[letter_index]
            if word is not '':
                if (formatted_input[letter_index] is ' ') or (letter_index is (input_length - 1)):
                    # Adds the assembled word into dictionary if it does not
                    # exist or increases the word count if the word exists
                    # in dictionary. Will adds the entry only if the word is
                    # not an empty string. and there is a space detected or
                    # if the loop has reached the end of the line.
                    if word not in word_count:
                        word_count[word] = 1.0
                    elif word in word_count:
                        word_count[word] += 1.0
                    word = ''
        break

    # First checks if the message type in was 'done', if it is the program will exit here and not
    # in the main program's while loop.
    if len(word_count) is 1:
        if 'done' in word_count:
            print('Thank you for using the document search system!')
            exit(0)

    # Constructing data structure to be the same as the one outputted by process_book()
    user_input = {'user input': word_count}
    return user_input


def get_doc_names(dictionary, config_file_name):
    """
    This function takes in a dictionary (empty or not) and opens a file
    specified by the variable 'config_file' and takes each line in config
    file and treats it as an input of a filename to open later on for
    processing. So each of the file names will be a key to the dictionary
    and each of those values will itself be a dictionary to store the word
    count. This function should not be processing any text files, but
    be filled with empty dictionaries.

    Data Structure:
    outer dictionaries: books = {}
    inner dictionaries: wells = {}, stevenson = {}, dickens {}

    books = {wells: {'word': word_count}, stevenson: {'word': word_count}, dickens: {'word': word_count}}
    """

    text_doc = None
    try:
        text_doc = open(config_file_name, 'r')
    except IOError:
        print('ERROR: Filename "{0}" does not exist in the same directory as this program.'.format(config_file_name))
        exit(-2)

    for line_in_text_doc in text_doc:
        # Formats the line by removing the new line character and attempt
        # to test the file name by attempting to open it and then promptly
        # close it.
        file_name = line_in_text_doc.replace("\n", "")

        if file_name in dictionary:
            # This is a case where if dictionary passed in was not empty and
            # the key contains some values, it will guarantee it will not be
            # overwritten and be destroyed.
            print('WARNING: Filename "{0}" is duplicated in config file "{1}".\n'.format(file_name, config_file_name))
            continue

        file_name_tester = None
        try:
            file_name_tester = open(file_name, 'r')
        except IOError:
            print('ERROR: Filename "{0}" does not exist in the same directory as this program. '
                  'Recheck your config file "{1}".'.format(file_name, config_file_name))
            exit(-2)
        file_name_tester.close()

        # If opening it works, then a dictionary is created for this file and
        # both the information will be stored into dictionary passed into this
        # function. The file name will be the key and the new dictionary created
        # will be the value.
        sub_dictionary = dict()
        dictionary[file_name] = sub_dictionary
    text_doc.close()
    return dictionary


def delete_punctuation(s):
    """
    This function removes all the punctuation from the string.
    It will replace the punctuation with a space to prevent problems
    such as bliu13@ucsc.edu from being merged into bliu13ucscedu
    """
    new_s = s
    for p in '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~':
        new_s = new_s.replace(p, " ")

    if punctuation[6] in new_s:
        new_s = new_s.replace(punctuation[6], "")
    return new_s


def process_book(book_file):
    """
    This function reads in a book and returns a dictionaries:
    one with the count of all words (not just the top thirty).

    words = process_book('jekyll_and_hyde.txt')

    This function does not guard against situations where numbers are embedded
    in words. The numbers are simply omitted.
    """

    # Create dictionaries for word count and letter count
    word_count = dict()

    # Opening the file whose filename is the parameter
    text_doc = None
    try:
        text_doc = open(book_file, 'r')
    except IOError:
        print('ERROR: Filename "{0}" does not exist in the same directory as this program.'.format(book_file))
        exit(-1)

    for line_in_text_doc in text_doc:
        # Formats the line by removing the punctuation and lower casing everything
        line = line_in_text_doc.lower()
        line = delete_punctuation(line)

        # Loops through the line over each character at a time
        word = ''
        line_length = len(line)
        for line_index in range(line_length):
            if 'a' <= line[line_index] <= 'z':
                # If current character the index is on in the line is a
                # letter, then add the letter to the word
                word += line[line_index]
            if word is not '':
                if (line[line_index] is ' ') or (line_index is (line_length - 1)):
                    # Adds the assembled word into dictionary if it does not
                    # exist or increases the word count if the word exists
                    # in dictionary. Will adds the entry only if the word is
                    # not an empty string. and there is a space detected or
                    # if the loop has reached the end of the line.
                    if word not in word_count:
                        word_count[word] = 1.0
                    elif word in word_count:
                        word_count[word] += 1.0
                    word = ''
                    continue
    text_doc.close()
    return word_count


def update_book(dictionary, book_file):
    """
    This function reads in a book and updates a dictionary:
    one with the count of all words (not just the top thirty).

    words = process_book('jekyll_and_hyde.txt')

    This function does not guard against situations where numbers are embedded
    in words. The numbers are simply omitted. There really isn't
    any use for this function but it is added in anyway.
    """

    # Create dictionaries for word count and letter count
    word_count = dictionary

    # Opening the file whose filename is the parameter
    text_doc = None
    try:
        text_doc = open(book_file, 'r')
    except IOError:
        print('ERROR: Filename "{0}" does not exist in the same directory as this program.'.format(book_file))
        exit(-1)

    for line_in_text_doc in text_doc:
        # Formats the line by removing the punctuation and lower casing everything
        line = line_in_text_doc.lower()
        line = delete_punctuation(line)

        # Loops through the line over each character at a time
        word = ''
        line_length = len(line)
        for line_index in range(line_length):
            if 'a' <= line[line_index] <= 'z':
                # If current character the index is on in the line is a
                # letter, then add the letter to the word
                word += line[line_index]
            if word is not '':
                if (line[line_index] is ' ') or (line_index is (line_length - 1.0)):
                    # Adds the assembled word into dictionary if it does not
                    # exist or increases the word count if the word exists
                    # in dictionary. Will adds the entry only if the word is
                    # not an empty string. and there is a space detected or
                    # if the loop has reached the end of the line.
                    if word not in word_count:
                        word_count[word] = 1.0
                    elif word in word_count:
                        word_count[word] += 1.0
                    word = ''
                    continue
    text_doc.close()
    return word_count


def similarity(d1, d2, length_d1, length_d2):
    """
    This function computes the cosine similarity of documents
    d1 and d2 (specified by dictionaries) using the lengths
    provided.

    The input is in a form of a dictionary containing many
    sub dictionaries. The dictionary d1 is designated as input
    from the user input and this function is structured in such a
    way that it treats d1 as if it was just 1 dictionary being
    compared to d2. If any sub-dictionary in d1 gives a high ranking
    when compared to d2, it will simply be inserted into the
    similarity_ranking dictionary and the lowest entry is removed if
    there are more than 20 entries.
    """
    # Create new dict to store the highest 20 of the most similar
    # with the key being the name of the document and the value
    # being their level of similarity.
    similarity_ranking = {}
    for book1 in d1:
        # Iterate through each dictionary of text in d1, but since
        # d1 is user input and by default it only has 1 sub-dictionary
        # it will finish after looping through once.
        for book2 in d2:
            # Iterate through each sub-dictionary in d2, which are
            # the books processed by process_book()
            cross_product = 0.0
            for word in d1[book1]:
                # Iterate through each word of the user input
                try:
                    product = d1[book1][word] * d2[book2][word]
                    cross_product += product / (length_d1[book1] * length_d2[book2])
                except KeyError:
                    # Word does not exist in this particular book in d2
                    # processed from process_book()
                    continue
            if len(similarity_ranking) <= 20:
                similarity_ranking[book2] = cross_product
            if len(similarity_ranking) > 20:
                # If more than 20 keys so see if we can trash 1 of them, but
                # it will first add the newest entry in first and set the newest
                # entry as the one to remove. It will iterate through the entire
                # dictionary for the next smallest similarities value.
                similarity_ranking[book2] = cross_product
                key_to_remove = book2
                value_to_remove = cross_product
                for book3 in similarity_ranking:
                    if similarity_ranking[book3] < value_to_remove:
                        key_to_remove = book3
                        value_to_remove = similarity_ranking[book3]
                del similarity_ranking[key_to_remove]
    return similarity_ranking


def find_occurrence_and_count(dictionary):
    """
    Takes in a dictionary of filled with dictionaries and checks each sub-dictionary
    to see how many words exist and its word count in each type of sub-dictionary.
    This function will return a dictionary whose key is the word and the value being
    how many of those dictionaries/text files contain the word. You can think of this
    as merging multiple sub-dictionaries and counting how many words are in each
    sub-dictionary and the total count that exist.
    """
    # Creates two new dictionary to store word occurrences and word count
    occurrence_count = dict()
    word_count = dict()

    # Iterate through each book in the dictionary
    for book in dictionary:
        # Iterate through each word in the sub-dictionary
        for word in dictionary[book]:
            # Increments count on word if this book has the word
            # if not, add the word entry into the dictionary
            if word in occurrence_count:
                occurrence_count[word] += 1.0
                word_count[word] += dictionary[book][word]
            elif word not in occurrence_count:
                occurrence_count[word] = 1.0
                word_count[word] = dictionary[book][word]
    return occurrence_count, word_count


def normalize_counts(d, tf_idf):
    """
    This function takes in a dictionary that contains the IDF that are
    already calculated by compute_idf(). It then calculates for the
    TF_IDF by multiplying the frequency of the word in a text document
    and by the IDF. The length of the document is found by summing the
    squares of the TF-IDF values of all the words in the dictionary
    and then square rooting that sum. The other parameter passed in, d
    is the original dictionary that contains the original word count values
    generated by process_book().

    Will return a dictionary that will contain the length of the vector
    for each text document.
    """
    # This code is necessary because a 'deep copy' is necessary.
    # copying the outer dictionary will cause the new dictionary's
    # keys to be pointed to the old dictionary's key's values.
    vector_lengths_dic = d.copy()
    for book in d:
        vector_lengths_dic[book] = d[book].copy()

    # This is where we iterate through each book and calculates the length
    # of the vector in that book and it into the dictionary where the key
    # is the document name and the value is the length of the vector.
    for book in tf_idf:
        sum_of_squared = 0.0
        for word in tf_idf[book]:
            sum_of_squared += tf_idf[book][word] ** 2.0
        vector_lengths_dic[book] = sqrt(sum_of_squared)
    return vector_lengths_dic


def compute_tf_idf(d, idf):
    """
    This function takes in a dictionary that contains the IDF that are
    already calculated by compute_idf(). It then calculates for the
    TF_IDF by multiplying the frequency of the word in a text document
    and by the IDF. The other dictionary, d is the original dictionary
    that contains the word count generated by process_book().

    This function will return a dictionary containing sub-dictionaries
    whose values are changed
    """
    # This code is necessary because a 'deep copy' is necessary.
    # copying the outer dictionary will cause the new dictionary's
    # keys to be pointed to the old dictionary's key's values.
    tf_idf_dic = d.copy()
    for book in d:
        tf_idf_dic[book] = d[book].copy()

    # This calculates the tf-idf, which is the idf * frequency of word in
    # the particular book contained in the sub-dictionary. Results are
    # stored in the sub-dictionary of tf_idf_dic.
    for book in tf_idf_dic:
        for word in tf_idf_dic[book]:
            weighted_idf = idf[word] * d[book][word]
            tf_idf_dic[book][word] = weighted_idf
    return tf_idf_dic


def compute_idf(dictionary):
    """
    This function will take in a dictionary filled with keys of different books
    and calculates the idf for each word. This function will return
    a dictionary with the word as the key and the value being the idf.
    """
    occurrence, count = find_occurrence_and_count(dictionary)
    number_of_docs = float(len(dictionary))

    idf_dic = dict()
    # Both the dictionaries: occurrence and count have the same exact
    # keys so we can pick either of them to iterate through. This calculates
    # the idf of the word.
    for word in occurrence:
        # ln(N / (n + 1)) where N = total docs, n = sum of existences in doc
        # same as ln(N) - ln(n + 1), this is to avoid getting negative numbers
        # from doing log on fractions
        numerator = log(number_of_docs)
        denominator = log(occurrence[word] + 1.0)
        idf_dic[word] = numerator - denominator
    return idf_dic


if __name__ == '__main__':
    # Process books
    config_file = get_config_name()
    dict_of_books = {}
    dict_of_books = get_doc_names(dict_of_books, config_file)
    for current_book in dict_of_books:
        dict_of_books[current_book] = process_book(current_book)
    computed_idf_dic = compute_idf(dict_of_books)
    computed_tf_idf_dic = compute_tf_idf(dict_of_books, computed_idf_dic)
    length_of_dic = normalize_counts(dict_of_books, computed_tf_idf_dic)

    while True:
        # Process user input, process_user_input() outputs the same data
        # structure as process_book() so it can use all the same functions
        user_input_dic = process_user_input()
        computed_idf_user_input_dic = compute_idf(user_input_dic)
        computed_tf_idf_user_input_dic = compute_tf_idf(user_input_dic, computed_idf_user_input_dic)
        length_of_user_input_dic = normalize_counts(user_input_dic, computed_tf_idf_user_input_dic)

        # Find similarities
        similarity_dic = similarity(computed_tf_idf_user_input_dic, computed_tf_idf_dic,
                                    length_of_user_input_dic, length_of_dic)
        # Sort the dictionary by its values and now proceeding to print out the book
        # with the largest similarity value. The results are in a list so we need to
        # create list of the same order with the book names.
        sorted_similarity_values = sorted(similarity_dic.values())
        sorted_similarity_values.reverse()
        print('Your document results are:')
        for value in sorted_similarity_values:
            for key in similarity_dic:
                if similarity_dic[key] is value:
                    print('{0}:        {1}'.format(key, similarity_dic[key]))